﻿using DBConnect.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBConnect
{
    public partial class FrmLogIn : Form
    {
        Connection cnt;

        public FrmLogIn()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (txtUser.Text.Equals("") || txtPass.Text.Equals(""))
            {
                MessageBox.Show("Error, fields are empty. ");
            }
            else
            {
                cnt = new Connection(txtUser.Text, txtPass.Text);

                if (cnt.connect.State == ConnectionState.Open)
                {
                    //MessageBox.Show("Connection established. ");
                    txtUser.Text = "";
                    txtPass.Text = "";
                    MDIPrincipal frm = new MDIPrincipal(cnt);
                    frm.Show();
                    this.Hide();
                }
                else
                {
                    MessageBox.Show("User o password incorrect. ");
                }
            }
        }

        private void txtUser_Enter(object sender, EventArgs e)
        {
            txtUser.Text = "";
        }

        private void txtPass_Enter(object sender, EventArgs e)
        {
            txtPass.Text = "";
            txtPass.PasswordChar = '*';
        }
    }
}

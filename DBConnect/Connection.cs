﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBConnect
{
    public class Connection
    {
        public SqlConnection connect;

        public Connection(string user, string pass)
        {
            try
            {
                connect = new SqlConnection($"Server=.;Database=DBConnect;UID={user};pwd={pass}");
                connect.Open();
            }
            catch (Exception)
            {

            }
        }

        public DataTable ChargeValues(string sp)
        {
            SqlCommand cmd = new SqlCommand();

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = sp;
            cmd.Connection = connect;

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            DataTable dt = new DataTable();
            da.Fill(dt);

            return dt;
        }

        public void ExportToExcel(DataGridView dgv)
        {
            Microsoft.Office.Interop.Excel.Application dataExp = new Microsoft.Office.Interop.Excel.Application();

            dataExp.Application.Workbooks.Add(true);

            int columnIndex = 0;

            foreach (DataGridViewColumn column in dgv.Columns)
            {
                columnIndex++;

                dataExp.Cells[1, columnIndex] = column.Name;
            }

            int rowIndex = 0;

            foreach (DataGridViewRow row in dgv.Rows)
            {
                rowIndex++;
                columnIndex = 0;

                foreach (DataGridViewColumn column in dgv.Columns)
                {
                    columnIndex++;
                    dataExp.Cells[rowIndex + 1, columnIndex] = row.Cells[column.Name].Value;
                }
            }

            dataExp.Visible = true;
        }
    }
}

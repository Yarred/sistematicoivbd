﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBConnect.Views
{
    public partial class FrmRegistry : Form
    {
        Connection cnt;

        public FrmRegistry()
        {
            InitializeComponent();
        }

        public FrmRegistry(Connection cnt)
        {
            this.cnt = cnt;
            InitializeComponent();

            string[] cmbItems = { "1", "2"};
            cmbList.DataSource = cmbItems;
            dgvTable.DataSource = cnt.ChargeValues("VerDepartamentos");
        }


        private void cmbList_SelectionChangeCommitted(object sender, EventArgs e)
        {
            switch (cmbList.SelectedIndex)
            {
                case 0:
                    dgvTable.DataSource = cnt.ChargeValues("VerDepartamentos");
                    break;

                case 1:
                    dgvTable.DataSource = cnt.ChargeValues("VerMunicipios");
                    break;

            }
        }

        private void btnExcel_Click(object sender, EventArgs e)
        {
            cnt.ExportToExcel(dgvTable);
        }

        private void btnReport_Click(object sender, EventArgs e)
        {
            switch (cmbList.SelectedIndex)
            {
                case 0:
                    break;

                case 1:
                    break;

            }
        }
    }
}

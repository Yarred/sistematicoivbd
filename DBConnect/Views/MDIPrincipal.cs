﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBConnect.Views
{
    public partial class MDIPrincipal : Form
    {
        Connection cnt;

        public MDIPrincipal()
        {
            InitializeComponent();
        }

        public MDIPrincipal(Connection cnt)
        {
            this.cnt = cnt;
            InitializeComponent();
        }

        private void miTable_Click(object sender, EventArgs e)
        {
            FrmRegistry frm = new FrmRegistry(cnt);
            frm.MdiParent = this;
            frm.Show();
        }

        private void MDIPrincipal_FormClosed(object sender, FormClosedEventArgs e)
        {

            cnt.connect.Close();
            Application.Exit();
        }
    }
}
